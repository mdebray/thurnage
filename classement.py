from html.parser import HTMLParser

INPUT_FILE = "classement.html"
OUTPUT_DIR = "classement"


class RegistrationParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.registered = []
        self.in_td = False

    def handle_starttag(self, tag, attrs):
        if tag == "tr":
            self.registered.append([])
            return
        if tag == "td":
            self.in_td = True
            return

    def handle_endtag(self, tag):
        if tag == "td":
            self.in_td = False
            return
        if tag == "tr" and len(self.registered[-1]) != 4:
            self.registered.pop()

    def handle_data(self, data):
        if self.in_td:
            self.registered[-1].append(data)

    def feedFromFile(self, path):
        with open(path) as f:
            self.feed(f.read())


htmlparser = RegistrationParser()
htmlparser.feedFromFile(INPUT_FILE)
categories = {}
for i in htmlparser.registered:
    if i[3] not in categories:
        categories[i[3]] = []
    categories[i[3]].append((i[1], i[2]))
    print(i[3], (i[1], i[2]))

for cat in categories:
    with open(f"{OUTPUT_DIR}/{cat}", "w") as f:
        for c in categories[cat]:
            f.write(f"{c[0]}\n")
